package pl.kayzone.networkeddemos;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class LocalSrvConfig {

    @Value("${result-path}")
    private String resultPath;
}
