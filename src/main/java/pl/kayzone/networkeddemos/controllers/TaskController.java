package pl.kayzone.networkeddemos.controllers;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kayzone.networkeddemos.services.FileService;
import pl.kayzone.networkeddemos.services.FileServiceImpl;

@RestController
@RequiredArgsConstructor
public class TaskController {

    public static final String TASK_API_PATH = "/api/task";

    private final FileService fileService;

    @GetMapping(TASK_API_PATH)
    public void saveJsonPlaceholderToFiles() throws BadRequestException {
        Try.run(fileService::savePlaceholderToFile)
            .getOrElseThrow(ex -> new BadRequestException(FileServiceImpl.ERROR_MESSAGE));
    }
}
