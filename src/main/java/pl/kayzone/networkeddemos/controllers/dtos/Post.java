package pl.kayzone.networkeddemos.controllers.dtos;

public record Post(Long id,
                   Long userId,
                   String title,
                   String body) {
}
