package pl.kayzone.networkeddemos.services;

import io.vavr.control.Try;
import java.util.List;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;
import pl.kayzone.networkeddemos.controllers.dtos.Post;

@Service
public class JsonPlaceholderConnectorImpl implements JsonPlaceholderConnector {

    private final RestClient.Builder builder;

    @Autowired
    public JsonPlaceholderConnectorImpl(@Qualifier("configureRestClientBuilder") RestClient.Builder builder) {
        this.builder = builder;
    }

    @Override
    public List<Post> getRemoteListOfPosts() throws BadRequestException {
        RestClient client = builder.build();
        return Try.of(() -> client.get()
                .retrieve()
                .body(new ParameterizedTypeReference<List<Post>>() {}))
                           .getOrElseThrow(ex -> new BadRequestException("Cannot get values from remote host"));
    }
}
