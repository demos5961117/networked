package pl.kayzone.networkeddemos.services;

import java.io.IOException;
import java.util.List;
import pl.kayzone.networkeddemos.controllers.dtos.Post;

public interface FileService {

    void saveToFiles(List<Post> objects, String paths) throws IOException;

    void savePlaceholderToFile() throws IOException;

}
