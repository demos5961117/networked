package pl.kayzone.networkeddemos.services;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import pl.kayzone.networkeddemos.RemoteSrvConfig;

@Configuration
@RequiredArgsConstructor
public class HttpConfigurator {

    private final RemoteSrvConfig configs;

    @Bean
    public RestClient.Builder configureRestClientBuilder() {
        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory(configs.getUrl() + configs.getPostEndpoint());
        return RestClient.builder()
                        .uriBuilderFactory(factory)
                        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    }
}
