package pl.kayzone.networkeddemos.services;

import java.util.List;
import org.apache.coyote.BadRequestException;
import pl.kayzone.networkeddemos.controllers.dtos.Post;

public interface JsonPlaceholderConnector {

    List<Post> getRemoteListOfPosts() throws BadRequestException;

}
