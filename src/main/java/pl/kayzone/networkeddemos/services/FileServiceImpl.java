package pl.kayzone.networkeddemos.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Try;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.boot.json.JsonParseException;
import org.springframework.stereotype.Service;
import pl.kayzone.networkeddemos.LocalSrvConfig;
import pl.kayzone.networkeddemos.controllers.dtos.Post;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final ObjectMapper objectMapper;

    private final JsonPlaceholderConnector connector;

    private final LocalSrvConfig config;

    public static final String ERROR_MESSAGE = "cannot execute task";

    @Override
    public void saveToFiles(List<Post> objects, String path) throws IOException {
        Path p = Path.of(path);
        if (!Files.exists(p)) {
            Files.createDirectory(p);
        }
        objects.forEach(o -> saveFile(o, p.toFile().getAbsolutePath()));
    }

    @Override
    public void savePlaceholderToFile() throws IOException {
        Try.of(connector::getRemoteListOfPosts)
            .andThenTry(l -> saveToFiles(l, config.getResultPath()))
            .onFailure(log -> Logger.getLogger(this.getClass().toString()).log(Level.INFO, ERROR_MESSAGE))
            .getOrElseThrow( ex -> new BadRequestException(ERROR_MESSAGE));
    }


    private void saveFile(Post object, String path){
        if (object == null) {
            return;
        }
        Path p;
        if (path.trim().isEmpty()) {
            p =  Paths.get(object.id() + ".json");
        } else {
            p = Paths.get(path + FileSystems.getDefault().getSeparator() + object.id() + ".json");
        }

        Try.of(() -> objectMapper.writeValueAsString(object))
            .andThenTry(o -> Files.writeString(p, o))
            .getOrElseThrow(ex -> new JsonParseException(new Throwable("cannot write object as string")));
    }
}
