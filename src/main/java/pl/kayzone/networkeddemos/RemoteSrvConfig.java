package pl.kayzone.networkeddemos;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("remote-server")
public class RemoteSrvConfig {

    private String url;

    private String postEndpoint;

}
