package pl.kayzone.networkeddemos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("pl.kayzone")
public class NetworkedDemosApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetworkedDemosApplication.class, args);
    }

}
