package pl.kayzone.networkeddemos;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.RestClient;

@Configuration
@RequiredArgsConstructor
public class AppConfigs {

    private final RemoteSrvConfig config;

    @Bean
    public ObjectMapper objectMapper() {
        return Jackson2ObjectMapperBuilder.json().build();
    }

    @Bean
    public RestClient restClient(RestClient.Builder builder) {
        return builder
            .baseUrl(config.getUrl())
            .build();
    }

    @Bean
    public RestClientCustomizer restClientCustomizer() {
        return restClientBuilder ->  restClientBuilder
            .requestFactory(new JdkClientHttpRequestFactory())
            .baseUrl(config.getUrl());
    }
}
