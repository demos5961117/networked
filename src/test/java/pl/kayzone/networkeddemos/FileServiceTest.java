package pl.kayzone.networkeddemos;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import pl.kayzone.networkeddemos.controllers.dtos.Post;
import pl.kayzone.networkeddemos.services.FileService;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class FileServiceTest extends  BaseTest {

    @Autowired
    private FileService fileService;

    private final LocalSrvConfig config;

    private JacksonTester<Post> json;

    private JacksonTester<List<Post>> jsonList;

    private final static String exampleResourceFileName = "example-response.json";

    @Autowired
    FileServiceTest(ObjectMapper objectMapper,
                    LocalSrvConfig config) throws IOException {
        super(objectMapper, exampleResourceFileName);
        this.config = config;
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    void should_get_list_of_object_and_save_to_files() throws IOException {
        List<Post> postList = this.jsonList.readObject(resourceFile);

        fileService.saveToFiles(postList, config.getResultPath());
        File fistFilePath =
            Path.of(config.getResultPath() + FileSystems.getDefault().getSeparator() + "1.json").toFile();

        String firstJson = Files.readString(fistFilePath.toPath());
        assertThat(json.parse(firstJson).getObject())
            .usingRecursiveComparison()
            .isEqualTo(postList.get(0));
    }

    @Test
    void should_throw_exception_if_cannot_save_to_file() throws IOException {
        List<Post> expactedList = this.jsonList.readObject(resourceFile);
        Assertions.assertThatThrownBy(
                                () -> fileService.saveToFiles(expactedList, "/////"))
            .isInstanceOf(JsonParseException.class);
    }
}
