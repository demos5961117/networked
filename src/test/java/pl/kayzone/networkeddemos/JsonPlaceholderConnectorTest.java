package pl.kayzone.networkeddemos;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.coyote.BadRequestException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatusCode;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClient;
import pl.kayzone.networkeddemos.controllers.dtos.Post;
import pl.kayzone.networkeddemos.services.JsonPlaceholderConnector;
import pl.kayzone.networkeddemos.services.JsonPlaceholderConnectorImpl;

@RestClientTest(JsonPlaceholderConnector.class)
@ExtendWith(MockitoExtension.class)
class JsonPlaceholderConnectorTest extends BaseTest {

    private JsonPlaceholderConnectorImpl connector;

    @SpyBean
    RestClient.Builder builder;

    private JacksonTester<List<Post>> json;

    private final RemoteSrvConfig config;

    private final static String exampleResourceFileName = "example-response.json";

    @Autowired
    public JsonPlaceholderConnectorTest(ObjectMapper objectMapper,
                                        RemoteSrvConfig configClass) throws IOException {
        super(objectMapper, exampleResourceFileName);
        this.config = configClass;
    }
    @BeforeEach
    void init() {
        super.mockServer = MockRestServiceServer
            .bindTo(builder)
            .ignoreExpectOrder(true)
            .build();
        this.connector = new JsonPlaceholderConnectorImpl(this.builder);
    }

    @AfterEach
    void after() {
        super.getMockServer().reset();
    }


    @Test
    void should_return_error_when_JsonPlaceholder_list_is_empty() throws URISyntaxException {
        super.expectedResponse(config.getUrl() + config.getPostEndpoint(),
                               super.expactedJsonString, HttpStatusCode.valueOf(400));

        Assertions.assertThrows(BadRequestException.class, () -> connector.getRemoteListOfPosts());
    }

    @Test
    void should_get_PostsList_when_JsonPlaceholder_called() throws IOException, URISyntaxException {
        super.expectedResponse(config.getUrl() + config.getPostEndpoint(),
                               expactedJsonString, HttpStatusCode.valueOf(200));
        List<Post> result = connector.getRemoteListOfPosts();
        assertThat(this.json.write(result)).isEqualToJson(expactedJsonString);
    }
}
