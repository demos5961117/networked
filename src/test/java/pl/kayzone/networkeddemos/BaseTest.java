package pl.kayzone.networkeddemos;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Objects;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;

abstract class BaseTest {

    protected final ObjectMapper objectMapper;

    protected MockRestServiceServer mockServer;

    protected File resourceFile;

    protected String expactedJsonString;

    BaseTest(ObjectMapper objectMapper,
             String resourcePath) throws IOException {
        this.objectMapper = objectMapper;

        this.expactedJsonString = loadResources(resourcePath);
        JacksonTester.initFields(this, objectMapper);
    }

    protected String loadResources(String resourcePath) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        assert resourcePath != null;
        resourceFile = new File(Objects.requireNonNull(classLoader.getResource(resourcePath)).getFile());
        return Files.readString(resourceFile.toPath());
    }

    protected void expectedResponse(String uri, String body, HttpStatusCode status) throws URISyntaxException {
        this.mockServer.expect(ExpectedCount.manyTimes(),
                          requestTo(new URI(uri)) )
                  .andExpect(method(HttpMethod.GET))
                  .andRespond(withStatus(status)
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(body, StandardCharsets.UTF_8));
    }

    protected MockRestServiceServer getMockServer() {
        return mockServer;
    }
}
