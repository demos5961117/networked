package pl.kayzone.networkeddemos;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.kayzone.networkeddemos.controllers.dtos.Post;
import pl.kayzone.networkeddemos.controllers.TaskController;
import pl.kayzone.networkeddemos.services.FileService;
import pl.kayzone.networkeddemos.services.JsonPlaceholderConnector;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class TaskControllerTest extends BaseTest {

    @MockBean
        private FileService fileService;

    @MockBean
        private JsonPlaceholderConnector connector;

    private MockMvc mockMvc;

    private JacksonTester<List<Post>> json;

    private final static String exampleResourceFileName = "example-response.json";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    TaskControllerTest(ObjectMapper objectMapper) throws IOException {
        super(objectMapper, exampleResourceFileName);
    }

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        JacksonTester.initFields(this, objectMapper);
        mockMvc = MockMvcBuilders
            .webAppContextSetup(this.webApplicationContext)
            .build();
    }

    @Test
    void should_invoke_rest_method() throws Exception {
        List<Post> expectedResult = this.json.readObject(resourceFile);

        Mockito.when(connector.getRemoteListOfPosts()).thenReturn(expectedResult);
        doNothing().when(fileService).saveToFiles(any(), any());
        MockHttpServletRequestBuilder requestBuilder = get(TaskController.TASK_API_PATH)
            .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk())
            .andReturn();
    }
}
