plugins {
    java
    id("org.springframework.boot") version "3.2.5"
    id("io.spring.dependency-management") version "1.1.4"
}

group = "pl.kayzone"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

var lombokVersion = "1.18.32"
var junitJupieterVersion = "5.10.2"
var platvormVersion = "1.10.2"
var vavrVersion = "0.10.4"
var mockitoVersion = "5.11.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.projectlombok:lombok:${lombokVersion}")
    implementation("io.vavr:vavr:${vavrVersion}")
    compileOnly("org.projectlombok:lombok:${lombokVersion}")
    annotationProcessor("org.projectlombok:lombok:${lombokVersion}")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:${junitJupieterVersion}")
    testImplementation("org.junit.jupiter:junit-jupiter:${junitJupieterVersion}")
    testImplementation("org.junit.platform:junit-platform-launcher:${platvormVersion}")
    testImplementation("org.mockito:mockito-junit-jupiter:${mockitoVersion}")
}

tasks.withType<Test> {
    useJUnitPlatform()
    jvmArgs("-Xshare:off")
}
